<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$name = $address = $salary = "";
$name_err = $address_err = $salary_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate name
    $newinput = '';
    $input_name = trim($_POST["name"]);
    if(!empty($input_name)){
        $str_arr = explode(' ', $input_name);
        $umur = preg_replace( '/[^\d]/', '', $str_arr);
        //val kota
        $kota = end(array_values($str_arr));
        //val usia
        $usia = end(array_filter($umur));
        
        $newinput = str_replace('th', '', str_replace('tahun', '', $input_name));
        $newstr = str_replace ( $umur," ", $newinput );
        $newnama = str_replace(end(array_values($str_arr)), '', $newstr) ;
        //var_dump($newnama);

    }
 

    
    // Check input errors before inserting in database
    if(!empty($input_name)){
        // Prepare an insert statement
        $sql = "INSERT INTO biodata (nama, kota, usia) VALUES ('".$newnama."', '".$kota."','".$usia."')";
        //var_dump($sql);die;
         
       if($result = $conn->prepare($sql)){
           
            // Attempt to execute the prepared statement
            if($result->execute()){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5">Create Record</h2>
                    <p>Please fill this form and submit to add data record to the database.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group">
                            <label>Input field</label>
                            <input type="text" id= "name" name="name" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                            <span class="invalid-feedback"><?php echo $name_err;?></span>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>
