<?php
/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */

/*return
    [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=mariadb-server;dbname=webusagi',
        'username' => 'webdev',
        'password' => 'webdev@dk',
        'charset' => 'utf8',
    ];*/


/*define('DB_SERVER', '127.0.0.1');
define('DB_USERNAME', 'webdev');
define('DB_PASSWORD', 'webdev@dk');
define('DB_NAME', 'webusagi');
 
/* Attempt to connect to MySQL database 
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
*/

$servername = "mariadb-server";
$username = "webdev";
$password = "webdev@dk";

try {
  $conn = new PDO("mysql:host=$servername;dbname=webusagi", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //echo "Connected successfully";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>